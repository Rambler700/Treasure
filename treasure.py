# -*- coding: utf-8 -*-
"""
Created on Sun Dec 17 22:00:03 2017

@author: enovi
"""
##License: GPL 3.0

## Treasure
import random

def main():
    choice = 'y'
    while choice == 'y':
        print("Welcome to Treasure!")
        treasure()
        choice = input('New game y/n? ')
        if choice == 'y':
            print ('Starting new game')
            choice = 'y'
        if choice == 'Y':
            print ('Starting new game')
            choice = 'y'

def treasure():
    count = 0
    guess1 = 0
    guess2 = 0
    finalguess = 0
    mapsize = 0
    numguesses = 0
    
    print("Can you guess where the treasure is located?")
    #set difficulty
    mapsize = int(input("The map size will be X by X. Select X: "))
    numguesses = int(input("Select the number of guesses: "))
    # store coordinates in a list
    location = random.randint(0,(mapsize * mapsize - 1))
    locy = int(location / (mapsize - 1))
    locx = location - (locy * (mapsize - 1))
     
    while count < numguesses:
        print("You have {} guesses left".format(numguesses - count))
        print("Map size is {} by {}".format(mapsize,mapsize))
        guess1 = int(input("Pick the X coordinate: "))
        guess2 = int(input("Pick the Y coordinate: "))
        finalguess = guess1 + (mapsize * guess2)

        print(guess1,',',guess2)
        if location == finalguess:
            print('You found the treasure')
            return
        else:
            print('You did not find the treasure')
            count += 1
    if count == numguesses:
        print("You ran out of guesses")
        print("The location was",locx,",",locy)
        return

main()        